package main

import (
	"fmt"
	"time"
)

type Person struct {
	Name string
	Age  int
}

func main() {

	var John Person
	fmt.Printf("%T %#v \n", John, John)

	// fields accessing (доступ к полям)
	John.Name = "John"
	John.Age = 30
	fmt.Println(John)

	// create with named field (создание с названиями полей структуры)
	Brad := Person{
		Name: "Brad",
		Age:  25,
	}
	fmt.Println(Brad)

	// create without field names (создание без имен полей)
	Vladimir := Person{"Vladimir", 40}
	fmt.Println(Vladimir)

	// field accessing through the pointer
	pVladimir := &Vladimir
	fmt.Println((*pVladimir).Age)
	fmt.Println(pVladimir.Age)

	// create pointer to struct
	pIvan := &Person{"Ivan", 90}
	fmt.Println(pIvan)

	unnamedStruct := struct {
		Name, LastName, BirthDate string
	}{
		Name:      "NoName",
		LastName:  "NoLastName",
		BirthDate: fmt.Sprintf("%s", time.Now()),
	}
	fmt.Println(unnamedStruct)

}

/* 0. кастомный тип - это тип который мы создаем сами

1. Что такое структура? Структура - это набор полей

2. Объявление структуры
	- структура объявляется с использованием ключевого слова type, далее указывается название типа и указывается ее тип struct (type StructName struct)
	- в фигурных скобках (внутри структуры) мы указываем поля, название поля и его тип

		type StructName struct {
			fieldName fieldType
			field2Name field2Type

3. Инициализация структуры
	- 1. инициализация структуры с дефолтным значением.
		для инициализации структуры необходимо создать переменную, указать ее имя и указать ее тип (StructName)
		var Name StructName
	- 2. второй вариант инициализации - Name = StructName {} (имя переменной = (равно) тип (StructName) с фигурными скобками)

4. Доступ к полям через указатель на структуры

5. Создание указателя на структуры

6. Анонимные структуры




*/
