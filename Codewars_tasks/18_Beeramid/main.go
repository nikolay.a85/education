package main

import "fmt"

func main() {
	var bonus int
	var price float64

	fmt.Scanf("%d %f", &bonus, &price)

	fmt.Println(Beeramid(bonus, price))
}

func Beeramid(bonus int, price float64) (count int) {

	temp := float64(bonus)

	for i := float64(1); temp >= price*(i*i); i++ {
		temp -= price * (i * i)
		count++
	}

	return
}
