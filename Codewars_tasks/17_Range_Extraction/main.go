package main

import (
	"fmt"
	"strconv"
)

func main() {
	list := []int{-6, -4, -3, -2, -1, 0, 1, 3, 4, 5, 7, 8, 9, 10, 11, 14, 15, 17, 18, 19, 20, 22, 23}

	res := Solution(list)
	fmt.Println(res)
}

func Solution(list []int) string {
	str := strconv.Itoa(list[0])

	for i := 1; i < len(list); i++ {
		switch {
		case list[i]-list[i-1] != 1 || i+1 < len(list) && list[i]-list[i-1] == 1 && list[i+1]-list[i] != 1:
			str += "," + strconv.Itoa(list[i])
		case i+1 < len(list) && list[i]-list[i-1] == 1 && list[i+1]-list[i] == 1:
			for j := i + 1; j < len(list) && list[j]-list[j-1] == 1; j++ {
				i = j
			}
			str += "-" + strconv.Itoa(list[i])
		default:
			str += "," + strconv.Itoa(list[i])
		}

	}

	return str
}
