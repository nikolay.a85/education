package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func convertToDecimal(arr []string) int {
	var res int
	var s float64 = 3
	for _, el := range arr {
		value, err := strconv.Atoi(el)
		if err != nil {
			panic(err)
		}
		res += value * int(math.Pow(256, s))
		s--
	}
	return res
}

func IpsBetween(start, end string) int {
	arrStart := strings.Split(start, ".")
	arrEnd := strings.Split(end, ".")

	return convertToDecimal(arrEnd) - convertToDecimal(arrStart)
}

func main() {
	var start, end string = "20.0.0.10", "20.0.1.0"

	fmt.Println(IpsBetween(start, end))

}
