package main

import (
	"fmt"
	"sync"
)

func Merge(c ...chan string) <-chan string {
	out := make(chan string)

	var wg sync.WaitGroup

	wg.Add(len(c))

	for _, ch := range c {
		go func(a chan string) {
			for msg1 := range a {
				out <- msg1
			}
			wg.Done()
		}(ch)

	}
	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

func main() {
	a := make(chan string, 3)
	a <- "a-foo"
	a <- "a-bar"
	a <- "a-baz"
	close(a)

	// channel b contains 2 messages
	b := make(chan string, 2)
	b <- "b-foo"
	b <- "b-bar"
	close(b)

	// channel c contains 2 messages
	c := make(chan string, 2)
	c <- "c-foo"
	c <- "c-bar"
	close(c)

	// channel d contains 3 messages
	d := make(chan string, 3)
	d <- "d-foo"
	d <- "d-bar"
	d <- "d-baz"
	close(d)

	out := Merge(a, b, c, d)
	for msg := range out {
		fmt.Println(msg)
	}
}
