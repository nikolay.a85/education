package main

import (
	"fmt"
)

func nbYear(percent float64, p0, aug, p int) int {
	count := 0

	for p0 < p {
		add := int(float64(p0) * (percent / 100))
		p0 = p0 + add + aug
		count++
	}
	return count
}

func main() {
	var p0, aug, p int
	var percent float64
	fmt.Scanf("%d %f %d %d", &p0, &percent, &aug, &p)

	fmt.Println(nbYear(percent, p0, aug, p))
}
