package main

import "fmt"

func ChooseBestSum(t, k int, ls []int) int {
	var maxSum int = -1
	var dfs func(int, int, int)

	dfs = func(currentSum int, currentIndex int, citiesVisited int) {
		if citiesVisited == k {
			if currentSum <= t && currentSum > maxSum {
				maxSum = currentSum
			}
			return
		}

		for i := currentIndex; i < len(ls); i++ {
			dfs(currentSum+ls[i], i+1, citiesVisited+1)
		}
	}

	dfs(0, 0, 0)

	if maxSum == -1 {
		return -1
	}

	return maxSum
}

func main() {
	var ls = []int{91, 74, 73, 85, 73, 81, 87}
	var t, k = 331, 4

	fmt.Println(ChooseBestSum(t, k, ls))

}
