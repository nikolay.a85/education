package main

import "testing"

type testCase struct {
	a, b   int
	expect int
}

func TestGetSum(t *testing.T) {
	var testCases = []testCase{
		{0, 1, 1},
		{1, 2, 3},
		{5, -1, 14},
		{505, 4, 127759},
		{321, 123, 44178},
		{0, -1, -1},
		{-50, 0, -1275},
		{-1, -5, -15},
		{-5, -5, -5},
		{-505, 4, -127755},
		{-321, 123, -44055},
		{0, 0, 0},
		{-5, -1, -15},
		{5, 1, 15},
		{-17, -17, -17},
		{17, 17, 17},
	}

	for _, tCase := range testCases {
		v := GetSum(tCase.a, tCase.b)
		if v != tCase.expect {
			t.Fatal("For", tCase.a, tCase.b,
				"expected", tCase.expect,
				"got", v,
			)
		}
	}
}
