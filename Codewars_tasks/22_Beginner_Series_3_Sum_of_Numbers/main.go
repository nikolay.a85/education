package main

import "fmt"

func GetSum(a, b int) int {
	if a > b {
		a, b = b, a
	}
	var sum = a
	for i := a + 1; i <= b; i++ {
		sum += i
	}
	return sum
}

func main() {
	var a, b int
	fmt.Scanf("%d %d", &a, &b)

	fmt.Println(GetSum(a, b))
}
