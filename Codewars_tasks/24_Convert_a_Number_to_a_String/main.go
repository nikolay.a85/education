package main

import "fmt"

func NumberToString(n int) string {
	return fmt.Sprintf("%d", n)
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	fmt.Println(NumberToString(n))
}
