package main

import (
	"fmt"
	"strings"
)

func ReverseWords(str string) string {
	arr := strings.Split(str, " ")
	for i, word := range arr {
		var revWord string
		for j := 0; j <= len(word)-1; j++ {
			revWord += string(word[(len(word)-1)-j])
		}
		arr[i] = revWord
	}

	return strings.Join(arr, " ")
}

func main() {
	str := "stressed desserts"

	fmt.Println(ReverseWords(str))
}
