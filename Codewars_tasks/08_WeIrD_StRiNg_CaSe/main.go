package main

import (
	"fmt"
	"strings"
)

func toWeirdCase(str string) string {
	arr := strings.Split(str, " ")
	for i, word := range arr {
		var s string
		for j, el := range []byte(word) {

			if j%2 == 0 {
				s += strings.ToUpper(string(el))
			} else {
				s += strings.ToLower(string(el))
			}
		}
		arr[i] = s
	}
	return strings.Join(arr, " ")
}

func main() {
	var str string
	fmt.Scanf("%s", &str)

	fmt.Println(toWeirdCase(str))
}
