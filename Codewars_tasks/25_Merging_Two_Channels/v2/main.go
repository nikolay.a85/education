package main

import (
	"fmt"
	"sync"
)

func Merge(a <-chan string, b <-chan string) <-chan string {
	ch := make(chan string)

	var wg sync.WaitGroup

	wg.Add(2)
	go func() {
		for msg1 := range a {
			ch <- msg1

		}
		wg.Done()
	}()

	go func() {
		for msg2 := range b {
			ch <- msg2
		}
		wg.Done()
	}()

	go func() {
		wg.Wait()
		close(ch)
	}()

	return ch
}

func main() {
	// channel a contains 3 messages
	a := make(chan string, 3)
	a <- "foo"
	a <- "bar"
	a <- "baz"
	close(a)

	// channel b contains 2 messages
	b := make(chan string, 2)
	b <- "hello"
	b <- "world"
	close(b)

	ch := Merge(a, b)
	for msg := range ch {
		fmt.Println(msg)
	}
}
