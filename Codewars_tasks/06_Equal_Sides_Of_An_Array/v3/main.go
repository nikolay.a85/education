package main

import "fmt"

type ArrayOfNumbers struct {
	arr []int
}

func newArr(x []int) *ArrayOfNumbers {
	return &ArrayOfNumbers{
		arr: x,
	}
}

func (n *ArrayOfNumbers) findIndex() int {
	var sumLeft int

	for i, _ := range n.arr {
		var sumRight int
		for j := i + 1; j < len(n.arr); j++ {
			sumRight += n.arr[j]
		}
		if sumLeft == sumRight {
			return i
		}
		sumLeft += n.arr[i]
	}

	return -1
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	arr := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&arr[i])
	}

	x := newArr(arr)
	fmt.Println(x.findIndex())
}
