package main

import "fmt"

func sumElements(arr []int) int {
	var total int
	for _, el := range arr {
		total += el
	}

	return total
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	arr := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&arr[i])
	}

	res := -1
	for i := 0; i < len(arr); i++ {
		if sumElements(arr[:i]) == sumElements(arr[i+1:]) {
			res = i
		}
	}
	fmt.Println(res)
}
