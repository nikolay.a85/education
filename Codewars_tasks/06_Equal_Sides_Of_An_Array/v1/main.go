package main

import "fmt"

func FindEvenIndex(arr []int) int {
	var sumLeft int

	for i := 0; i < len(arr); i++ {
		var sumRight int
		for j := i + 1; j < len(arr); j++ {
			sumRight += arr[j]
		}
		if sumLeft == sumRight {
			return i
		}
		sumLeft += arr[i]
	}

	return -1
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	arr := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&arr[i])
	}

	fmt.Println(FindEvenIndex(arr))
}
