package main

import "fmt"

func main() {
	arr := []int{1, 1, 2, 2, 2, 1, 1}

	fmt.Println(Solution(arr))
}

func Solution(ar []int) int {
	arr := make([]int, len(ar))
	copy(arr, ar)

	if len(arr) < 1 {
		return 0
	}
	res := arr[0]

	for i := 1; i < len(arr); i++ {
		for arr[i] != 0 {
			res, arr[i] = arr[i], res%arr[i]
		}
	}

	return res * len(arr)
}
