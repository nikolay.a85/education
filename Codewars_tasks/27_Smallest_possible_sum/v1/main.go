package main

import (
	"fmt"
	"sort"
)

func main() {
	arr := []int{6, 9, 21}

	fmt.Println(Solution(arr))
}

func Solution(ar []int) int {
	slice := make([]int, len(ar))
	copy(slice, ar)

	if len(slice) == 0 {
		return 0
	} else if len(slice) == 1 {
		return slice[0]
	}
	sort.Slice(slice, func(i, j int) bool {
		return slice[i] > slice[j]
	})

	for slice[0] != slice[len(slice)-1] {
		slice[0] = slice[0] - slice[1]
		sort.Slice(slice, func(i, j int) bool {
			return slice[i] > slice[j]
		})
	}

	res := 0

	for _, v := range slice {
		res += v
	}

	return res
}
