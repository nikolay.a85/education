package main

import (
	"fmt"
	"strings"
)

func main() {
	var str = " test case"

	fmt.Println(CamelCase(str))
}

func CamelCase(s string) string {
	arr := strings.Split(s, " ")

	for i, word := range arr {
		if word == "" {
			continue
		}
		arr[i] = fmt.Sprintf("%s%s", strings.ToUpper(string(word[0])), word[1:])
	}

	return strings.Join(arr, "")
}
