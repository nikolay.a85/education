package main

import (
	"fmt"
	"math"
)

func IsPrime(n int) bool {
	if n > 1 {
		for i := 2; i < int(math.Sqrt(float64(n)))+1; i++ {
			if n%i == 0 {
				return false
			}
		}
		return true
	}
	return false
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	fmt.Println(IsPrime(n))
}
