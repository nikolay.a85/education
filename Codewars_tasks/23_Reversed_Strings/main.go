package main

import "fmt"

func main() {
	var str string
	fmt.Scanf("%s", &str)

	fmt.Println(Solution(str))
}

func Solution(word string) string {
	var newWord string

	for i := 1; i <= len(word); i++ {
		newWord += string(word[len(word)-i])
	}

	return newWord
}
