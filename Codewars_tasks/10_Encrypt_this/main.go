package main

import (
	"fmt"
	"strconv"
	"strings"
)

func EncryptThis(text string) string {
	arr := strings.Split(text, " ")
	for i, str := range arr {
		x := []byte(str)
		var newStr string
		for j := 0; j < len(x); j++ {
			switch {
			case j == 0:
				newStr += strconv.Itoa(int(x[j]))
			case j == 1:
				newStr += string(x[len(x)-1])
			case j == len(x)-1:
				newStr += string(x[1])
			default:
				newStr += string(x[j])
			}
		}
		arr[i] = newStr
	}
	return strings.Join(arr, " ")
}

func main() {
	var str string
	fmt.Scanln(&str)

	fmt.Println(EncryptThis(str))
}
