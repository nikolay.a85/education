package main

import (
	"fmt"
	"math"
)

func FindNb(m int) int {
	var n, sum int
	for sum < m {
		n++
		sum += int(math.Pow(float64(n), 3))
	}

	if sum == m {
		return n
	}

	return -1
}

func main() {
	var m int
	fmt.Scanf("%d", &m)

	fmt.Println(FindNb(m))
}
