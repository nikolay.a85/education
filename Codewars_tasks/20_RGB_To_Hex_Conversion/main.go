package main

import "fmt"

const min, max = 0, 255

func correctValue(v int) int {
	switch {
	case v < min:
		v = min
	case v > max:
		v = max
	}
	return v
}

func intToHex(v int) string {
	return fmt.Sprintf("%X%X", correctValue(v)/16, correctValue(v)%16)
}

func RGB(r, g, b int) string {
	return intToHex(r) + intToHex(g) + intToHex(b)
}

func main() {
	var red, green, blue int
	fmt.Scanf("%d %d %d", &red, &green, &blue)

	fmt.Println(RGB(red, green, blue))
}
