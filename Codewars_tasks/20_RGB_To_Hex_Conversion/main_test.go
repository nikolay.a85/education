package main

import "testing"

type testCase struct {
	R, G, B int
	expect  string
}

func TestRGB(t *testing.T) {
	var testCases = []testCase{
		{255, 255, 255, "FFFFFF"},
		{255, 255, 300, "FFFFFF"},
		{0, 0, 0, "000000"},
		{148, 0, 211, "9400D3"},
	}

	for _, tCase := range testCases {
		c := RGB(tCase.R, tCase.G, tCase.B)
		if c != tCase.expect {
			t.Fatal("For", tCase.R, tCase.G, tCase.B,
				"expected", tCase.expect,
				"got", c)
		}
	}
}
