package main

import (
	"fmt"
	"strconv"
)

func main() {
	var s = "048665088X"

	fmt.Print(ValidISBN10(s))
}

func ValidISBN10(isbn string) bool {
	var total, count int

	for i, symbol := range isbn {
		value, err := strconv.Atoi(string(symbol))
		if err != nil {
			if string(symbol) == "X" && i == 9 || string(symbol) == "x" && i == 9 {
				total += 10 * (i + 1)
				count++
			} else {
				return false
			}

		} else {
			total += value * (i + 1)
			count++
		}
	}

	if total%11 == 0 && count == 10 {
		return true
	}
	return false
}
