package main

import "fmt"

func main() {
	var n, add, subtract int
	fmt.Scanf("%d", &n)

	busStops := make([][2]int, 0)

	for i := 0; i < n; i++ {
		fmt.Scan(&add, &subtract)
		busStops = append(busStops, [2]int{add, subtract})
	}

	fmt.Print(Number(busStops))
}

func Number(busStops [][2]int) int {
	var number int
	for _, arr := range busStops {
		for i, value := range arr {
			if i%2 == 0 {
				number += value
			} else {
				number -= value
			}
		}
	}
	return number
}
