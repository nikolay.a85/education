package main

import "fmt"

func BouncingBall(h, bounce, window float64) int {
	count := -1

	if h > 0 && bounce > 0 && bounce < 1 && window < h {
		for h > window {
			h *= bounce
			count += 2
		}
		return count

	} else {
		return count
	}
}

func main() {
	var h, bounce, window float64
	fmt.Scanf("%f %f %f", &h, &bounce, &window)

	fmt.Println(BouncingBall(h, bounce, window))
}
