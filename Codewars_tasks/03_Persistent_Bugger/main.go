package main

import "fmt"

func Persistence(n int) int {
	count := 0

	for n >= 10 {
		dob := 1
		for n > 1 {
			dob *= n % 10
			n /= 10
		}
		n = dob
		count++
	}
	return count
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	fmt.Println(Persistence(n))
}
