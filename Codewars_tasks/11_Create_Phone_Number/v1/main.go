package main

import (
	"fmt"
	"strconv"
)

func CreatePhoneNumber(numbers [10]uint) string {
	var PhoneNumber string
	for _, v := range numbers {
		PhoneNumber += strconv.Itoa(int(v))
	}
	PhoneNumber = fmt.Sprintf("(%s) %s-%s", PhoneNumber[0:3], PhoneNumber[3:6], PhoneNumber[6:])

	return PhoneNumber
}

func main() {
	var arr [10]uint
	for i := 0; i < 10; i++ {
		fmt.Scan(&arr[i])
	}

	fmt.Println(CreatePhoneNumber(arr))
}
