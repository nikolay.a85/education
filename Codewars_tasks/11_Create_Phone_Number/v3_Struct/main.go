package main

import (
	"fmt"
	"strconv"
)

type Numbers struct {
	arr [10]uint
}

func (n *Numbers) createPhoneNumber() string {
	var PhoneNumber string
	for _, v := range n.arr {
		PhoneNumber += strconv.Itoa(int(v))
	}

	return fmt.Sprintf("(%s) %s-%s", PhoneNumber[0:3], PhoneNumber[3:6], PhoneNumber[6:])
}

func main() {
	var arr [10]uint
	for i := 0; i < 10; i++ {
		fmt.Scan(&arr[i])
	}

	x := Numbers{arr}

	fmt.Println(x.createPhoneNumber())
}
