package main

import "fmt"

func FindOutlier(integers []int) int {
	if len(integers) > 2 {
		var evenCount, oddCount, nEven, nOdd int

		for _, v := range integers {
			if v%2 == 0 {
				evenCount++
				nEven = v
			} else {
				oddCount++
				nOdd = v
			}
		}

		if evenCount > oddCount {
			return nOdd
		} else {
			return nEven
		}
	}

	return 0
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	integers := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&integers[i])
	}

	fmt.Println(FindOutlier(integers))
}
