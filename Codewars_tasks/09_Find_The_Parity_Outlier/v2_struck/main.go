package main

import "fmt"

type Integers struct {
	arr []int
}

func newIntegers() *Integers {
	var n int
	fmt.Scanf("%d", &n)

	arr := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&arr[i])
	}

	return &Integers{
		arr,
	}
}

func (i *Integers) FindOutlier() int {
	if len(i.arr) > 2 {
		var evenCount, oddCount, nEven, nOdd int

		for _, v := range i.arr {
			if v%2 == 0 {
				evenCount++
				nEven = v
			} else {
				oddCount++
				nOdd = v
			}
		}

		if evenCount > oddCount {
			return nOdd
		} else {
			return nEven
		}
	}

	return 0
}

func main() {
	res := newIntegers()

	fmt.Println(res.FindOutlier())
}
