package main

import (
	"fmt"
	"strings"
)

func FirstNonRepeating(str string) string {
	newStr := strings.ToLower(str)

	for i, uSymbol := range newStr {
		count := 0
		for _, symbol := range newStr {
			if symbol == uSymbol {
				count++
			}
		}
		if count == 1 {
			return string(str[i])
		}
	}
	return ""
}

func main() {
	var str = "sTreSS"

	fmt.Println(FirstNonRepeating(str))
}
