package main

import "fmt"

func main() {
	var n, k int
	fmt.Scanf("%d %d", &n, &k)

	fmt.Println(JosephusSurvivor(n, k))
}

func JosephusSurvivor(n, k int) int {
	circle := make([]int, n)

	for i := range circle {
		circle[i] = i + 1
	}

	return SearchSurvivor(circle, k, 0)
}

func SearchSurvivor(circle []int, k, count int) int {
	for i := 0; i < len(circle) && len(circle) != 1; i++ {
		count++
		if count == k {
			circle = append(circle[0:i], circle[i+1:]...)
			count = 0
			i--
		}
	}

	if len(circle) == 1 {
		return circle[0]
	}

	return SearchSurvivor(circle, k, count)
}
