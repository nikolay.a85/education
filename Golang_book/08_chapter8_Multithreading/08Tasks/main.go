package main

import (
	"fmt"
	"time"
)

func Sleep(seconds int) {
	<-time.After(time.Duration(seconds) * time.Second)
	fmt.Println("End")
}
func main() {
	go Sleep(15)

	var input string
	fmt.Scanln(&input)

}
