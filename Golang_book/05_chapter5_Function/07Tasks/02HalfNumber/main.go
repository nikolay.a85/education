package main

import (
	"fmt"
)

func halfNumber(number int) (int, bool) {
	half := number / 2
	if half%2 == 0 {
		return half, true
	} else {
		return half, false
	}
}

func main() {
	number := 2
	fmt.Println(halfNumber(number))
}
