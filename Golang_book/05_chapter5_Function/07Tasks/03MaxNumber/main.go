package main

import "fmt"

func maxNumber(args ...int) int {
	max := 0
	for _, v := range args {
		if max < v {
			max = v
		}
	}
	return max
}

func main() {
	fmt.Println(maxNumber(1, 3, 5, 4, 12, 5, 7))
}
