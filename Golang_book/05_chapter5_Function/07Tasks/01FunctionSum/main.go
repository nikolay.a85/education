package main

import "fmt"

func sum(slice []int) int {
	var total int
	for _, v := range slice {
		total += v
	}
	return total
}

func main() {
	slice := []int{5, 15, 25, 5}
	fmt.Println(sum(slice))
}
