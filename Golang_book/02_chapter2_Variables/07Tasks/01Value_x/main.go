package main

import "fmt"

var x float64

func main() {
	x := 5
	fmt.Println(x)
	x += 1
	fmt.Println(x)
}
