package main

import "fmt"

func main() {
	arr := make([]int, 0)
	work(arr)
	fmt.Println(arr)
	workWithPointer(&arr)
	fmt.Println(arr)
}

func work(arr []int) {
	for i := 0; i < 10; i++ {
		arr = append(arr, i)
	}
}

func workWithPointer(arr *[]int) {
	if arr == nil {
		return
	}
	for i := 0; i < 10; i++ {
		*arr = append(*arr, i)
	}
}
