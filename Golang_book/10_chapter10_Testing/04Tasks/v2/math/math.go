package math

// Average find mean in array of numbers
func Average(arr []float64) float64 {
	if len(arr) != 0 {
		total := float64(0)

		for _, x := range arr {
			total += x
		}

		return total / float64(len(arr))
	}
	return 0
}

// Max find the maximum in an array of numbers
func Max(arr []float64) float64 {
	max := float64(0)

	for _, x := range arr {
		if max < x {
			max = x
		}
	}
	return max
}

// Min find the minimum in an array of numbers
func Min(arr []float64) float64 {
	min := arr[0]

	for _, x := range arr {
		if min > x {
			min = x
		}
	}
	return min
}
