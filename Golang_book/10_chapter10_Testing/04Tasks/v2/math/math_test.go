package math

import "testing"

type testCase struct {
	values []float64
	expect float64
}

func doTest(t *testing.T, f func(arr []float64) float64, testCases []testCase) {
	for _, tCase := range testCases {
		v := f(tCase.values)
		if v != tCase.expect {
			t.Fatal(
				"For", tCase.values,
				"expected", tCase.expect,
				"got", v,
			)
		}
	}
}

func TestAverage(t *testing.T) {
	var testCases = []testCase{
		{[]float64{1, 2}, 1.5},
		{[]float64{1, 1, 1, 1, 1, 1}, 1},
		{[]float64{-1, 1}, 0},
		{[]float64{}, 0},
	}
	doTest(t, Average, testCases)
}

func TestMax(t *testing.T) {
	var testCases = []testCase{
		{[]float64{1, 2}, 2},
		{[]float64{1, 1, 1, 1, 1, 1}, 1},
		{[]float64{-1, 1}, 1},
	}
	doTest(t, Max, testCases)
}

func TestMin(t *testing.T) {
	var testCases = []testCase{
		{[]float64{1, 2}, 1},
		{[]float64{1, 1, 1, 1, 1, 1}, 1},
		{[]float64{-1, 1}, -1},
	}
	doTest(t, Min, testCases)
}
