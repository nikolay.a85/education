package main

import (
	"fmt"
	m "golang-book/Golang_book/10_chapter10_Testing/04Tasks/v1/math"
)

func main() {
	xs := []float64{1, 2, 3, 4}
	avg := m.Average(xs)
	max := m.Max(xs)
	min := m.Min(xs)

	fmt.Println(min, avg, max)
}
