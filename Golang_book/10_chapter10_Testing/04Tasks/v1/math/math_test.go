package math

import "testing"

type testpair struct {
	values []float64
	expect float64
}

func TestAverage(t *testing.T) {
	var tests = []testpair{
		{[]float64{1, 2}, 1.5},
		{[]float64{1, 1, 1, 1, 1, 1}, 1},
		{[]float64{-1, 1}, 0},
		{[]float64{}, 0},
	}

	for _, pair := range tests {
		v := Average(pair.values)
		if v != pair.expect {
			t.Fatal(
				"For", pair.values,
				"expected", pair.expect,
				"got", v,
			)
		}
	}
}

func TestMax(t *testing.T) {
	var tests = []testpair{
		{[]float64{1, 2}, 2},
		{[]float64{1, 1, 1, 1, 1, 1}, 1},
		{[]float64{-1, 1}, 1},
	}

	for _, pair := range tests {
		v := Max(pair.values)
		if v != pair.expect {
			t.Error(
				"For", pair.values,
				"expected", pair.expect,
				"got", v,
			)
		}
	}
}

func TestMin(t *testing.T) {
	var tests = []testpair{
		{[]float64{1, 2}, 1},
		{[]float64{1, 1, 1, 1, 1, 1}, 1},
		{[]float64{-1, 1}, -1},
	}

	for _, pair := range tests {
		v := Min(pair.values)
		if v != pair.expect {
			t.Error(
				"For", pair.values,
				"expected", pair.expect,
				"got", v,
			)
		}
	}
}
