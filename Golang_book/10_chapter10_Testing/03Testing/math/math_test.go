package math

import (
	"fmt"
	"testing"
)

func TestAverage(t *testing.T) {
	testCases := []struct {
		values  []float64
		average float64
	}{
		{[]float64{1, 2}, 1.5},
		{[]float64{1, 1, 1, 1, 1, 1}, 1},
		{[]float64{-1, 1}, 0},
	}

	for i, testCase := range testCases {
		t.Run(fmt.Sprintf("Test case %d", i), func(t *testing.T) {
			v := Average(testCase.values)
			if v != testCase.average {
				t.Error(
					"For", testCase.values,
					"expected", testCase.average,
					"got", v,
				)
			}
		})
	}
}
