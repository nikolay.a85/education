package main

import (
	"fmt"
	m "golang-book/Golang_book/09_chapter9_Packages_and_re_code_usage/02Tasks/math"
)

func main() {
	xs := []float64{1, 2, 3, 4}
	avg := m.Average(xs)
	max := m.Max(xs)
	min := m.Min(xs)
	fmt.Println(min, avg, max)

}
