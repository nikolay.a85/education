package math

// Average find mean in array of numbers
func Average(arr []float64) float64 {
	total := float64(0)

	for _, x := range arr {
		total += x
	}

	return total / float64(len(arr))
}
