package main

import (
	"fmt"
	"math"
)

func quadrangleArea(a, b, c, d, f float64) float64 {
	p1 := halfPerimeter(a, b, f)
	p2 := halfPerimeter(c, d, f)
	s1 := math.Sqrt(p1 * (p1 - a) * (p1 - b) * (p1 - f))
	s2 := math.Sqrt(p2 * (p2 - c) * (p2 - d) * (p2 - f))

	return s1 + s2
}
func halfPerimeter(a, b, c float64) float64 {

	p := (a + b + c) / 2
	return p
}

func main() {
	var a, b, c, d, f float64
	fmt.Scanf("%f %f %f %f %f", &a, &b, &c, &d, &f)

	s := quadrangleArea(a, b, c, d, f)
	fmt.Printf("%.4f", s)
}
