package main

import (
	"fmt"
	"math"
)

type quadrangle struct {
	a float64
	b float64
	c float64
	d float64
	f float64
}

func (q *quadrangle) Area() float64 {
	p1 := (q.a + q.b + q.f) / 2
	s1 := math.Sqrt(p1 * (p1 - q.a) * (p1 - q.b) * (p1 - q.f))
	p2 := (q.c + q.d + q.f) / 2
	s2 := math.Sqrt(p2 * (p2 - q.c) * (p2 - q.d) * (p2 - q.f))

	return s1 + s2
}

func main() {
	var q quadrangle
	fmt.Scanf("%f %f %f %f %f", &q.a, &q.b, &q.c, &q.d, &q.f)

	fmt.Printf("%.4f", q.Area())
}
