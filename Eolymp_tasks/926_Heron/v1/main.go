package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c, d, f float64
	fmt.Scanf("%f %f %f %f %f", &a, &b, &c, &d, &f)

	p1 := (a + b + f) / 2
	s1 := math.Sqrt(p1 * (p1 - a) * (p1 - b) * (p1 - f))
	p2 := (c + d + f) / 2
	s2 := math.Sqrt(p2 * (p2 - c) * (p2 - d) * (p2 - f))

	s := s1 + s2

	fmt.Printf("%.4f", s)
}
