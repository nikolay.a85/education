package main

import "fmt"

func main() {
	var n, minSum int
	fmt.Scanf("%d", &n)

	p100 := 100
	p20 := 30
	p1 := 2

	sum := (n / 100 * p100) + (n % 100 / 20 * p20) + (n % 20 * p1)
	switch {
	case sum > (n/100+1)*p100:
		minSum = (n/100 + 1) * p100
	case sum > (n/100*p100)+((n%100/20+1)*p20):
		minSum = (n / 100 * p100) + ((n%100/20 + 1) * p20)
	case n < 100 && sum > (n%100/20+1)*p20:
		minSum = (n%100/20 + 1) * p20
	default:
		minSum = sum
	}
	fmt.Println(minSum)
}
