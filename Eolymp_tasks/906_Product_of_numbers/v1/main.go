package main

import "fmt"

func main() {
	var n uint
	fmt.Scanf("%d", &n)
	a := n / 100
	b := (n / 10) % 10
	c := n % 10
	pn := a * b * c

	fmt.Println(pn)
}
