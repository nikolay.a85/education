package main

import "fmt"

func productNumber(n int) int {
	a := n / 100
	b := (n / 10) % 10
	c := n % 10
	return a * b * c
}
func main() {
	var n int
	fmt.Scanf("%d", &n)
	pn := productNumber(n)
	println(pn)
}
