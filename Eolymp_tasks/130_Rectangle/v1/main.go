package main

import "fmt"

func main() {
	var Ax, Ay, Bx, By, Cx, Cy, Dx, Dy float64
	fmt.Scanf("%f %f %f %f %f %f", &Ax, &Ay, &Bx, &By, &Cx, &Cy)

	sAB := (Bx-Ax)*(Bx-Ax) + (By-Ay)*(By-Ay)
	sBC := (Cx-Bx)*(Cx-Bx) + (Cy-By)*(Cy-By)
	sCA := (Cx-Ax)*(Cx-Ax) + (Cy-Ay)*(Cy-Ay)

	switch {
	case sAB == sBC+sCA:
		Dx = Ax + Bx - Cx
		Dy = Ay + By - Cy
	case sBC == sAB+sCA:
		Dx = Bx + Cx - Ax
		Dy = By + Cy - Ay
	case sCA == sAB+sBC:
		Dx = Ax + Cx - Bx
		Dy = Ay + Cy - By
	}
	fmt.Println(Dx, Dy)
}
