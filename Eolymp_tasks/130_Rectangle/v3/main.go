package main

import (
	"fmt"
)

type Point struct {
	x, y float64
}

func newPoint(a, b float64) *Point {
	return &Point{
		x: a,
		y: b,
	}
}

func (p *Point) distance(a *Point) float64 {
	return (p.x-a.x)*(p.x-a.x) + (p.y-a.y)*(p.y-a.y)
}

type Rectangle struct {
	sideA, sideB, sideC float64
	a, b, c             *Point
}

func (r *Rectangle) calculateSides() {
	r.sideA = r.a.distance(r.b)
	r.sideB = r.b.distance(r.c)
	r.sideC = r.c.distance(r.a)
}

func (r *Rectangle) findUnknownPoint() *Point {
	var x, y float64

	switch {
	case r.sideA == r.sideB+r.sideC:
		x = r.a.x + r.b.x - r.c.x
		y = r.a.y + r.b.y - r.c.y
	case r.sideB == r.sideA+r.sideC:
		x = r.b.x + r.c.x - r.a.x
		y = r.b.y + r.c.y - r.a.y
	case r.sideC == r.sideA+r.sideB:
		x = r.c.x + r.a.x - r.b.x
		y = r.c.y + r.a.y - r.b.y
	}

	return newPoint(x, y)
}

func main() {
	var R Rectangle
	var x1, y1, x2, y2, x3, y3 float64
	fmt.Scanf("%f %f %f %f %f %f ", &x1, &y1, &x2, &y2, &x3, &y3)

	R.a = newPoint(x1, y1)
	R.b = newPoint(x2, y2)
	R.c = newPoint(x3, y3)

	R.calculateSides()
	d := R.findUnknownPoint()

	fmt.Println(d.x, d.y)
}
