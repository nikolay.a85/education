package main

import "fmt"

func createSlice() []float64 {
	var n int
	fmt.Scanf("%d", &n)

	slice := make([]float64, 0, n)
	for i := 0; i < n; i++ {
		var x float64
		fmt.Scan(&x)
		slice = append(slice, x)
	}
	return slice
}
func firstValue(slice []float64) {
	for i, value := range slice {
		if value <= 2.5 {

			fmt.Printf("%d %.2f", i+1, value)
			return
		}
	}
	fmt.Println("Not found")
}

func main() {

	a := createSlice()
	firstValue(a)
}
