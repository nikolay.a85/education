package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%d", &n)

	slice := make([]float64, 0, n)
	for i := 0; i < n; i++ {
		var x float64
		fmt.Scan(&x)
		slice = append(slice, x)
	}
	for i, value := range slice {
		if value <= 2.5 {

			fmt.Printf("%d %.2f", i+1, value)
			return
		}
	}
	fmt.Println("Not Found")
}
