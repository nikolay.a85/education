package main

import "fmt"

func main() {
	var x, y float64

	fmt.Scanf("%f %f", &x, &y)

	fmt.Println(quarterCoordinate(x, y))

}

func quarterCoordinate(x, y float64) int {
	switch {
	case x > 0 && y > 0:
		return 1
	case x < 0 && y > 0:
		return 2
	case x < 0 && y < 0:
		return 3
	case x > 0 && y < 0:
		return 4
	default:
		return 0
	}

}
