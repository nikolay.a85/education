package main

import "fmt"

func main() {
	fmt.Printf("%.2f", max())
}

func max() float64 {
	var n int
	var mv float64
	fmt.Scanf("%d", &n)

	for i := 0; i < n; i++ {
		var x float64
		fmt.Scan(&x)
		if x < 0 {
			x = x * -1
		}
		if x > mv {
			mv = x
		}
	}
	return mv
}
