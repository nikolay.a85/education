package main

import "fmt"

func main() {
	var n int
	var max float64
	fmt.Scanf("%d", &n)

	for i := 0; i < n; i++ {
		var x float64
		fmt.Scan(&x)
		if x < 0 {
			x = x * -1
			if x > max {
				max = x
			}
		} else if x > max {
			max = x
		}
	}
	fmt.Printf("%.2f", max)
}
