package main

import "fmt"

func main() {
	var input, count uint64
	fmt.Scanf("%d", &input)

	if input == 0 {
		count = 1
	}

	for input > 0 {
		input = input / 10

		count++
	}
	fmt.Println(count)
}
