package main

import (
	"fmt"
	"strconv"
)

func main() {

	var v uint64
	fmt.Scanf("%d", &v)
	a := strconv.FormatUint(v, 10)

	fmt.Println(len(a))
}
