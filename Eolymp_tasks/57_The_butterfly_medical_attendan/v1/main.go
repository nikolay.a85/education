package main

import (
	"fmt"
	"math"
)

func main() {
	var x1, y1, x2, y2, z2 float64

	fmt.Scanf("%f %f\n%f %f %f", &x1, &y1, &x2, &y2, &z2)

	// Пошук відстані між точками
	F := math.Sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2 * z2))

	// Пошук фокусної відстані D=1/F
	D := 1 / F

	fmt.Printf("%.3f", D)
}
