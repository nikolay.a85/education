package main

import "fmt"

func main() {
	var n, sum int
	fmt.Scanf("%d", &n)

	min := 100
	max := -100

	for i := 0; i < n; i++ {
		var element int
		fmt.Scan(&element)
		if element < min {
			min = element
		}
		if element > max {
			max = element
		}
		sum = min + max
	}
	fmt.Println(sum)
}
