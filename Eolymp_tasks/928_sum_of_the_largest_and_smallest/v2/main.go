package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%d", &n)

	slice := make([]int, 0, n)
	for i := 0; i < n; i++ {
		var element int
		fmt.Scan(&element)
		slice = append(slice, element)
	}
	sum := minElement(slice) + maxElement(slice)
	fmt.Println(sum)
}

func maxElement(arr []int) int {
	max := -100
	for _, element := range arr {
		if element > max {
			max = element
		}
	}
	return max
}

func minElement(arr []int) int {
	min := 100
	for _, element := range arr {
		if element < min {
			min = element
		}
	}
	return min
}
