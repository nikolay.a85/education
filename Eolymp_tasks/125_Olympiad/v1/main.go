package main

import "fmt"

func main() {
	var h1, h2, m1, m2, s1, s2 int
	fmt.Scanf("%d %d %d\n%d %d %d", &h1, &m1, &s1, &h2, &m2, &s2)

	if s1 > s2 {
		m2--
		s2 += 60
	}
	if m1 > m2 {
		h2--
		m2 += 60
	}
	h := h2 - h1
	m := m2 - m1
	s := s2 - s1

	fmt.Println(h, m, s)
}
