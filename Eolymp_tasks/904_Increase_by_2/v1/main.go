package main

import (
	"fmt"
)

func main() {
	var n int
	fmt.Scan(&n)
	arr := make([]int, 0, n)

	for i := 0; i < n; i++ {
		var x int
		fmt.Scan(&x)
		if x >= 0 {
			x += 2
		}
		arr = append(arr, x)
	}
	fmt.Println(arr)
}
