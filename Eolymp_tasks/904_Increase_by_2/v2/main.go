package main

import "fmt"

func createAndIncreaseNumbers() []int {
	var n, x int
	slice := make([]int, 0)
	fmt.Scanf("%d", &n)
	for i := 0; i < n; i++ {
		fmt.Scan(&x)
		if x >= 0 {
			x += 2
		}
		slice = append(slice, x)
	}
	return slice
}
func main() {
	a := createAndIncreaseNumbers()
	fmt.Println(a)
}
