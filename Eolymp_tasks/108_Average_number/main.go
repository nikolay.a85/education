package main

import "fmt"

func main() {
	var a, b, c int
	fmt.Scanf("%d %d %d", &a, &b, &c)

	switch {
	case b > a && a > c || b < a && a < c:
		fmt.Println(a)
	case a > b && b > c || a < b && b < c:
		fmt.Println(b)
	case a > c && c > b || a < c && c < b:
		fmt.Println(c)
	}
}
