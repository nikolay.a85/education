package main

import "fmt"

func main() {
	var n, count int
	var total float64
	fmt.Scanf("%d", &n)

	for i := 0; i < n; i++ {
		var x float64
		fmt.Scan(&x)
		if x < 0 {
			count++
			total += x
		}
	}
	fmt.Printf("%d %.2f", count, total)
}
