package main

import "fmt"

func sumAndCountNegativeElements(x []float64) (int, float64) {
	var count int
	var sum float64

	for _, value := range x {
		if value < 0 {
			count++
			sum += value
		}
	}
	return count, sum
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	slice := make([]float64, 0, n)

	for i := 0; i < n; i++ {
		var value float64
		fmt.Scan(&value)
		slice = append(slice, value)
	}
	a, b := sumAndCountNegativeElements(slice)
	fmt.Printf("%d %.2f", a, b)
}
