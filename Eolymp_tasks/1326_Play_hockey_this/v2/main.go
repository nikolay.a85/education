package main

import "fmt"

func main() {
	var n, count int
	fmt.Scanf("%d", &n)

	if n < 3 {
		count = n
	} else {
		count = countTheWaysOfDistribution(n)
	}

	fmt.Println(count)
}
func countTheWaysOfDistribution(n int) int {
	var count int
	for gold := 1; gold <= n; gold++ {
		for silver := 1; silver <= n; silver++ {
			if silver != gold {
				for bronze := 1; bronze <= n; bronze++ {
					if bronze != gold && bronze != silver {
						count++
					}
				}
			}
		}
	}
	return count
}
