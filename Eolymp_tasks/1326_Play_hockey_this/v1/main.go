package main

import "fmt"

func main() {
	var n, count int
	fmt.Scanf("%d", &n)

	if n < 3 {
		count = n
	}
	for g := 1; g <= n; g++ {
		for s := 1; s <= n; s++ {
			if s != g {
				for b := 1; b <= n; b++ {
					if b != g && b != s {
						count++
					}
				}
			}
		}
	}
	fmt.Println(count)
}
