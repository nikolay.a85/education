package main

import "fmt"

func main() {
	var n int
	var sum, area float64
	fmt.Scanf("%d", &n)

	//((x i+1 + x i )/2) * (y i+1 - y i )

	arrX := make([]float64, 0, n)
	arrY := make([]float64, 0, n)
	for i := 0; i < n; i++ {
		var x, y float64
		fmt.Scan(&x, &y)
		arrX = append(arrX, x)
		arrY = append(arrY, y)
	}
	for i := 0; i < n; i++ {
		if i > 0 {
			sum = ((arrX[i] + arrX[i-1]) / 2) * (arrY[i] - arrY[i-1])
		} else {
			sum = ((arrX[i] + arrX[n-1]) / 2) * (arrY[i] - arrY[n-1])
		}
		area += sum
	}
	if area < 0 {
		area *= -1
	}
	fmt.Printf("%.3f", area)
}
