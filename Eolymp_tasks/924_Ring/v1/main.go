package main

import (
	"fmt"
	"math"
)

func main() {
	var S, R float64
	const pi = math.Pi

	fmt.Scanf("%f %f", &S, &R)
	r := math.Sqrt(R*R - (S / pi))

	fmt.Printf("%.2f", r)
}
