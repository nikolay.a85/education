package main

import (
	"fmt"
	"math"
)

type Point struct {
	x, y, z float64
}

type triangle struct {
	a, b, c float64
}

func (t *triangle) calculateSides(a, b, c Point) {
	t.a = distance(a, b)
	t.b = distance(b, c)
	t.c = distance(c, a)
}

func (t *triangle) area() float64 {
	p := (t.a + t.b + t.c) / 2
	s := math.Sqrt(p * (p - t.a) * (p - t.b) * (p - t.c))

	return s
}

func distance(a, b Point) float64 {
	return math.Sqrt((b.x-a.x)*(b.x-a.x) + (b.y-a.y)*(b.y-a.y) + (b.z-a.z)*(b.z-a.z))
}

type pyramid struct {
	t1, t2, t3, t4 triangle
}

func (p *pyramid) area() float64 {
	s := p.t1.area() + p.t2.area() + p.t3.area() + p.t4.area()

	return s
}

func main() {
	var a, b, c, s Point
	fmt.Scanf("%f %f %f\n%f %f %f\n%f %f %f\n%f %f %f", &a.x, &a.y, &a.z, &b.x, &b.y, &b.z, &c.x, &c.y, &c.z, &s.x, &s.y, &s.z)

	var t1, t2, t3, t4 triangle
	t1.calculateSides(a, b, c)
	t2.calculateSides(a, s, b)
	t3.calculateSides(b, s, c)
	t4.calculateSides(c, s, a)

	var p pyramid
	p.t1 = t1
	p.t2 = t2
	p.t3 = t3
	p.t4 = t4

	fmt.Printf("%.1f", p.area())
}
