package main

import (
	"fmt"
	"math"
)

func main() {
	var Ax, Ay, Az, Bx, By, Bz, Cx, Cy, Cz, Sx, Sy, Sz float64
	fmt.Scanf("%f %f %f\n%f %f %f\n%f %f %f\n%f %f %f", &Ax, &Ay, &Az, &Bx, &By, &Bz, &Cx, &Cy, &Cz, &Sx, &Sy, &Sz)

	AB := math.Sqrt((Bx-Ax)*(Bx-Ax) + (By-Ay)*(By-Ay) + (Bz-Az)*(Bz-Az))
	AS := math.Sqrt((Sx-Ax)*(Sx-Ax) + (Sy-Ay)*(Sy-Ay) + (Sz-Az)*(Sz-Az))
	BC := math.Sqrt((Cx-Bx)*(Cx-Bx) + (Cy-By)*(Cy-By) + (Cz-Bz)*(Cz-Bz))
	BS := math.Sqrt((Sx-Bx)*(Sx-Bx) + (Sy-By)*(Sy-By) + (Sz-Bz)*(Sz-Bz))
	CA := math.Sqrt((Ax-Cx)*(Ax-Cx) + (Ay-Cy)*(Ay-Cy) + (Az-Cz)*(Az-Cz))
	CS := math.Sqrt((Sx-Cx)*(Sx-Cx) + (Sy-Cy)*(Sy-Cy) + (Sz-Cz)*(Sz-Cz))

	pABC := (AB + BC + CA) / 2
	sABC := math.Sqrt(pABC * (pABC - AB) * (pABC - BC) * (pABC - CA))
	pASB := (AS + BS + AB) / 2
	sASB := math.Sqrt(pASB * (pASB - AS) * (pASB - BS) * (pASB - AB))
	pBSC := (BS + CS + BC) / 2
	sBSC := math.Sqrt(pBSC * (pBSC - BS) * (pBSC - CS) * (pBSC - BC))
	pCSA := (CS + AS + CA) / 2
	sCSA := math.Sqrt(pCSA * (pCSA - CS) * (pCSA - AS) * (pCSA - CA))

	sABCS := sABC + sASB + sBSC + sCSA
	fmt.Printf("%.1f", sABCS)
}
