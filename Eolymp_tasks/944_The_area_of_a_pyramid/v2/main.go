package main

import (
	"fmt"
	"math"
)

func distance(x1, y1, z1, x2, y2, z2, x3, y3, z3 float64) float64 {
	a := math.Sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1))
	b := math.Sqrt((x3-x2)*(x3-x2) + (y3-y2)*(y3-y2) + (z3-z2)*(z3-z2))
	c := math.Sqrt((x1-x3)*(x1-x3) + (y1-y3)*(y1-y3) + (z1-z3)*(z1-z3))

	return triangleArea(a, b, c)
}

func triangleArea(a, b, c float64) float64 {
	p := (a + b + c) / 2
	s := math.Sqrt(p * (p - a) * (p - b) * (p - c))

	return s
}

func pyramidArea(t1, t2, t3, t4 float64) float64 {
	return t1 + t2 + t3 + t4
}

func main() {
	var Ax, Ay, Az, Bx, By, Bz, Cx, Cy, Cz, Sx, Sy, Sz float64
	fmt.Scanf("%f %f %f\n%f %f %f\n%f %f %f\n%f %f %f", &Ax, &Ay, &Az, &Bx, &By, &Bz, &Cx, &Cy, &Cz, &Sx, &Sy, &Sz)

	st1 := distance(Ax, Ay, Az, Bx, By, Bz, Cx, Cy, Cz)
	st2 := distance(Ax, Ay, Az, Sx, Sy, Sz, Bx, By, Bz)
	st3 := distance(Bx, By, Bz, Sx, Sy, Sz, Cx, Cy, Cz)
	st4 := distance(Cx, Cy, Cz, Sx, Sy, Sz, Ax, Ay, Az)

	fmt.Printf("%.1f", pyramidArea(st1, st2, st3, st4))
}
