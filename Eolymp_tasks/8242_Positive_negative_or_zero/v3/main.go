package main

import "fmt"

type number int

func (n number) check() string {
	switch {
	case n < 0:
		return "Negative"
	case n == 0:
		return "Zero"
	default:
		return "Positive"
	}
}

func main() {
	var n number
	fmt.Scanf("%d", &n)

	fmt.Println(n.check())
}
