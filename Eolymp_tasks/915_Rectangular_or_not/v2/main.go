package main

import "fmt"

func main() {
	var len1, len2, len3 uint
	fmt.Scanf("%d %d %d", &len1, &len2, &len3)

	fmt.Println(rectangularOrNot(len1, len2, len3))
}

func rectangularOrNot(a, b, c uint) string {

	a = a * a
	b = b * b
	c = c * c

	if a == b+c || b == a+c || c == a+b {
		return "YES"
	}
	return "NO"
}
