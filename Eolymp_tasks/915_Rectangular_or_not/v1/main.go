package main

import "fmt"

func main() {
	var a, b, c uint
	fmt.Scanf("%d %d %d", &a, &b, &c)

	a = a * a
	b = b * b
	c = c * c

	switch {
	case a == b+c:
		fmt.Println("YES")
	case b == a+c:
		fmt.Println("YES")
	case c == a+b:
		fmt.Println("YES")
	default:
		fmt.Println("NO")
	}
}
