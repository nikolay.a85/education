package main

import (
	"fmt"
	"math"
)

func amountBatteries(h, w, l, k float64) int {
	return int(math.Ceil(h * w * l / k))
}

func main() {
	var h, w, l, k float64

	fmt.Scanf("%f %f %f %f", &h, &w, &l, &k)

	n := amountBatteries(h, w, l, k)
	fmt.Println(n)
}
