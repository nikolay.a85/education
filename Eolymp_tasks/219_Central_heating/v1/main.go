package main

import (
	"fmt"
	"math"
)

func main() {
	var h, w, l, k float64
	fmt.Scanf("%f %f %f %f", &h, &w, &l, &k)

	//var amountBatteries float64
	//amountBatteries = h * w * l / k

	//fmt.Println(math.Ceil(amountBatteries))
	fmt.Println(int(math.Ceil(h * w * l / k)))
}
