package main

import (
	"fmt"
	"math"
)

func main() {
	var x, y, z float64
	fmt.Scanf("%f %f %f", &x, &y, &z)

	res := min(max(x, y), max(y, z), x+y+z)
	fmt.Printf("%.2f", res)
}

func max(a, b float64) float64 {
	if a > b || a == b {
		return a
	}
	return b
}
func min(a, b, c float64) float64 {
	arr := []float64{a, b, c}
	min := math.MaxFloat64
	for _, f := range arr {
		if f < min {
			min = f
		}
	}
	return min
}
