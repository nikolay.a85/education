package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%d", &n)

	a, b, c := decomposingANumber(n)
	fmt.Printf("%d\n%d\n%d", a, b, c)
}
func decomposingANumber(x int) (int, int, int) {
	if x < 0 {
		x = x * -1
	}

	a := x / 100
	b := (x / 10) % 10
	c := x % 10

	return a, b, c
}
