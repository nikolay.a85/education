package main

import "fmt"

func main() {
	var n, a, b, c int
	fmt.Scanf("%d", &n)

	if n < 0 {
		n = n * -1
	}

	a = n / 100
	b = (n / 10) % 10
	c = n % 10

	fmt.Printf("%d\n%d\n%d", a, b, c)
}
