package main

import "fmt"

type Square struct {
	side uint
}

func newSquare(a uint) *Square {
	return &Square{
		side: a,
	}
}

func (s *Square) perimeter() uint {
	return s.side * 4
}

func (s *Square) area() uint {
	return s.side * s.side
}

func main() {
	var n uint
	_, err := fmt.Scanf("%d", &n)
	if err != nil {
		return
	}
	t := newSquare(n)
	fmt.Println(t.perimeter(), t.area())
}
