package main

import "fmt"

func main() {
	var n uint

	for {
		_, err := fmt.Scanf("%d", &n)
		if err != nil {
			return
		}
		fmt.Println(n*4, n*n)
	}
}
