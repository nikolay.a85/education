package main

import "fmt"

func divisibleBy6(n int) (int, int) {
	var count, total int
	for i := 0; i < n; i++ {
		var x int
		fmt.Scan(&x)
		if x%6 == 0 && x > 0 {
			count++
			total += x
		}
	}
	return count, total
}
func main() {
	var n int
	fmt.Scanf("%d", &n)

	count, total := divisibleBy6(n)
	fmt.Println(count, total)
}
