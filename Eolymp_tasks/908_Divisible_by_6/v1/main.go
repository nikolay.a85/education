package main

import "fmt"

func main() {
	var n, count, total int
	fmt.Scanf("%d", &n)
	for i := 0; i < n; i++ {
		var x int
		fmt.Scan(&x)
		if x%6 == 0 && x > 0 {
			count++
			total += x
		}
	}
	fmt.Println(count, total)
}
