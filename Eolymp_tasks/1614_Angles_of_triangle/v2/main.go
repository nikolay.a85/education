package main

import (
	"fmt"
	"math"
)

func maxAngle(a, b, c float64) float64 {
	var mAngle float64
	switch {
	case a > b && a > c:
		mAngle = a
	case b > a && b > c:
		mAngle = b
	case c > a && c > b:
		mAngle = c
	}
	return mAngle
}

func findTheAngle(a, b, c float64) float64 {
	angle := math.Acos((a*a+b*b-c*c)/(2*a*b)) * (180 / math.Pi)
	return angle
}

func main() {
	var Ax, Ay, Bx, By, Cx, Cy float64
	fmt.Scanf("%f %f\n%f %f\n%f %f", &Ax, &Ay, &Bx, &By, &Cx, &Cy)

	a := math.Sqrt((Bx-Ax)*(Bx-Ax) + (By-Ay)*(By-Ay))
	b := math.Sqrt((Cx-Bx)*(Cx-Bx) + (Cy-By)*(Cy-By))
	c := math.Sqrt((Ax-Cx)*(Ax-Cx) + (Ay-Cy)*(Ay-Cy))

	angleA := findTheAngle(a, c, b)
	angleB := findTheAngle(a, b, c)
	angleC := findTheAngle(b, c, a)

	fmt.Printf("%.6f", maxAngle(angleA, angleB, angleC))
}
