package main

import (
	"fmt"
	"math"
)

func main() {
	var Ax, Ay, Bx, By, Cx, Cy, maxAngle float64
	fmt.Scanf("%f %f\n%f %f\n%f %f", &Ax, &Ay, &Bx, &By, &Cx, &Cy)

	AB := math.Sqrt((Bx-Ax)*(Bx-Ax) + (By-Ay)*(By-Ay))
	BC := math.Sqrt((Cx-Bx)*(Cx-Bx) + (Cy-By)*(Cy-By))
	CA := math.Sqrt((Ax-Cx)*(Ax-Cx) + (Ay-Cy)*(Ay-Cy))

	angleA := math.Acos((AB*AB+CA*CA-BC*BC)/(2*AB*CA)) * (180 / math.Pi)
	angleB := math.Acos((AB*AB+BC*BC-CA*CA)/(2*AB*BC)) * (180 / math.Pi)
	angleC := math.Acos((BC*BC+CA*CA-AB*AB)/(2*BC*CA)) * (180 / math.Pi)

	if angleA > angleB && angleA > angleC {
		maxAngle = angleA
	} else if angleB > angleA && angleB > angleC {
		maxAngle = angleB
	} else if angleC > angleA && angleC > angleB {
		maxAngle = angleC
	}

	fmt.Printf("%.6f", maxAngle)
}
