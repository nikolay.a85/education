package main

import "fmt"

func main() {
	var n, x, count int
	var price float64

	fmt.Scanf("%d", &n)

	for i := 0; i < n; i++ {
		fmt.Scan(&x, &price)
		if price < 50 {
			count += x
		}
	}
	fmt.Println(count)
}
