package main

import "fmt"

func main() {
	var a, b, c, d float64
	fmt.Scanf("%f %f %f %f", &a, &b, &c, &d)

	res := parallelogram(a, b, c, d)
	fmt.Println(res)
}

func parallelogram(a, b, c, d float64) string {
	var Y, N = "YES", "NO"
	switch {
	case a == b && c == d:
		return Y
	case a == c && b == d:
		return Y
	case a == d && b == c:
		return Y
	default:
		return N
	}
}
