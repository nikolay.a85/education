package main

import "fmt"

type parallelogram struct {
	a float64
	b float64
	c float64
	d float64
}

func (p *parallelogram) figure() string {
	switch {
	case p.a == p.b && p.c == p.d:
		return "YES"
	case p.a == p.c && p.b == p.d:
		return "YES"
	case p.a == p.d && p.b == p.c:
		return "YES"
	default:
		return "NO"
	}
}

func main() {
	var p parallelogram
	fmt.Scanf("%f %f %f %f", &p.a, &p.b, &p.c, &p.d)

	fmt.Println(p.figure())
}
