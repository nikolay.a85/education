package main

import "fmt"

type parallelogram struct {
	a float64
	b float64
	c float64
	d float64
}
type square struct {
	a float64
	b float64
	c float64
	d float64
}

func (p *parallelogram) figure() string {
	switch {
	case p.a == p.b && p.c == p.d:
		return "YES"
	case p.a == p.c && p.b == p.d:
		return "YES"
	case p.a == p.d && p.b == p.c:
		return "YES"
	default:
		return "NO"
	}
}

func (s *square) figure() string {
	switch {
	case s.a == s.b && s.c == s.d:
		return "YES"
	default:
		return "NO"
	}
}

func main() {
	var p parallelogram
	var s square
	fmt.Scanf("%f %f %f %f", &p.a, &p.b, &p.c, &p.d)
	s.a = p.a
	s.b = p.b
	s.c = p.c
	s.d = p.c

	fmt.Printf("parallelogram %s\n", p.figure())
	fmt.Printf("square %s", s.figure())
}
