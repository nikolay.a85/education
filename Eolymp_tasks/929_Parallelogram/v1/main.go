package main

import "fmt"

func main() {
	var a, b, c, d float64
	var Y, N = "YES", "NO"
	fmt.Scanf("%f %f %f %f", &a, &b, &c, &d)

	switch {
	case a == b && c == d:
		fmt.Println(Y)
	case a == c && b == d:
		fmt.Println(Y)
	case a == d && b == c:
		fmt.Println(Y)
	default:
		fmt.Println(N)
	}
}
