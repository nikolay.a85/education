package main

import "fmt"

func main() {
	var n, count int
	fmt.Scanf("%d", &n)

	for n > 0 {
		x := n
		sum := 0
		for x > 0 {
			sum += x % 10
			x = x / 10
		}
		n -= sum
		count++
	}
	fmt.Println(count)
}
