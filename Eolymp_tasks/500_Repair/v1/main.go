package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	var l, w, h float64
	fmt.Scanf("%d", &n)

	count := make([]float64, 0, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&l, &w, &h)
		x := (l*h*2 + w*h*2) / 16
		count = append(count, x)
	}
	for i := 0; i < n; i++ {
		fmt.Println(math.Ceil(count[i]))
	}
}
