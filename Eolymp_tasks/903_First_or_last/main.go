package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%d", &n)
	first := n / 100
	last := n % 10

	if first == last {
		fmt.Println("=")
	} else if first > last {
		fmt.Println(first)
	} else {
		fmt.Println(last)
	}
}
