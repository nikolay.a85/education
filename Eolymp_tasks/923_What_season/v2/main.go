package main

import "fmt"

func mapSeason(x int) string {

	elements := make(map[int]string)
	elements[3] = "Spring"
	elements[4] = "Spring"
	elements[5] = "Spring"
	elements[6] = "Summer"
	elements[7] = "Summer"
	elements[8] = "Summer"
	elements[9] = "Autumn"
	elements[10] = "Autumn"
	elements[11] = "Autumn"
	elements[12] = "Winter"
	elements[1] = "Winter"
	elements[2] = "Winter"

	return elements[x]
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	ws := mapSeason(n)
	fmt.Println(ws)
}
