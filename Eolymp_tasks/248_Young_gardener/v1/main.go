package main

import "fmt"

func main() {
	var n, l int
	fmt.Scanf("%d", &n)

	leaves := 2
	for i := 0; i < n; i++ {
		l += leaves
		leaves += 2

	}

	fmt.Println(l + 1)
}
