package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%d", &n)

	fmt.Printf("%.3f", theRatioOfProductToTheSum(n))
}
func theRatioOfProductToTheSum(x int) float64 {
	product := 1
	sum := 0

	for x > 0 {
		product *= x % 10
		sum += x % 10
		x = x / 10
	}
	ratio := float64(product) / float64(sum)
	return ratio
}
