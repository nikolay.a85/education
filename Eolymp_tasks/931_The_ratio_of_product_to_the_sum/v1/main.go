package main

import "fmt"

func main() {
	var n, sum int
	fmt.Scanf("%d", &n)

	dob := 1

	for n > 0 {
		dob *= n % 10
		sum += n % 10
		n = n / 10
	}
	fmt.Printf("%.3f", float64(dob)/float64(sum))
}
