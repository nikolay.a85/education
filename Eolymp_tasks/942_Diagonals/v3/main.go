package main

import (
	"fmt"
	"math"
)

type point struct {
	x float64
	y float64
}

type diagonal struct {
	ac, bd float64
}

func (p *point) intersectionCoordinates(a, c point) (float64, float64) {
	p.x = (a.x + c.x) / 2
	p.y = (a.y + c.y) / 2

	return p.x, p.y
}

func (di *diagonal) diagonalLength(a, b, c, d point) (float64, float64) {
	di.ac = math.Sqrt((c.x-a.x)*(c.x-a.x) + (c.y-a.y)*(c.y-a.y))
	di.bd = math.Sqrt((d.x-b.x)*(d.x-b.x) + (d.y-b.y)*(d.y-b.y))

	return di.ac, di.bd
}

func main() {
	var a, b, c, d, o point
	var q diagonal
	fmt.Scanf("%f %f\n%f %f\n%f %f\n%f %f", &a.x, &a.y, &b.x, &b.y, &c.x, &c.y, &d.x, &d.y)

	x, y := o.intersectionCoordinates(a, c)
	ac, bd := q.diagonalLength(a, b, c, d)

	fmt.Printf("%.3f %.3f\n%.3f %.3f", x, y, ac, bd)
}
