package main

import (
	"fmt"
	"math"
)

func diagonalIntersectionCoordinates(x1, y1, x2, y2 float64) (float64, float64) {
	x := (x1 + x2) / 2
	y := (y1 + y2) / 2

	return x, y
}

func diagonalLength(x1, y1, x2, y2 float64) float64 {
	dLen := math.Sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))

	return dLen
}

func main() {
	var Ax, Ay, Bx, By, Cx, Cy, Dx, Dy float64
	fmt.Scanf("%f %f\n%f %f\n%f %f\n%f %f", &Ax, &Ay, &Bx, &By, &Cx, &Cy, &Dx, &Dy)

	Ox, Oy := diagonalIntersectionCoordinates(Ax, Ay, Cx, Cy)
	AC := diagonalLength(Ax, Ay, Cx, Cy)
	BD := diagonalLength(Dx, Dy, Bx, By)

	fmt.Printf("%.3f %.3f\n%.3f %.3f", Ox, Oy, AC, BD)
}
