package main

import (
	"fmt"
	"math"
)

func main() {
	var Ax, Ay, Bx, By, Cx, Cy, Dx, Dy float64
	fmt.Scanf("%f %f\n%f %f\n%f %f\n%f %f", &Ax, &Ay, &Bx, &By, &Cx, &Cy, &Dx, &Dy)

	Ox := (Ax + Cx) / 2
	Oy := (Ay + Cy) / 2

	dAC := math.Sqrt((Cx-Ax)*(Cx-Ax) + (Cy-Ay)*(Cy-Ay))
	dBD := math.Sqrt((Dx-Bx)*(Dx-Bx) + (Dy-By)*(Dy-By))

	fmt.Printf("%.3f %.3f\n%.3f %.3f", Ox, Oy, dAC, dBD)
}
