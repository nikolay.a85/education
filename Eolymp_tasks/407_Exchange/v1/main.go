package main

import "fmt"

func main() {

	var t, k int
	fmt.Scanf("%d", &t)

	for i := 0; i < t; i++ {
		fmt.Scan(&k)

		left := "G"
		center := "C"
		right := "V"

		for a := 0; a < k; a++ {
			center, right = right, center
			left, center = center, left
		}
		fmt.Println(left + center + right)
	}
}
