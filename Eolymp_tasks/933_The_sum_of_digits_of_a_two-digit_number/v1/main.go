package main

import "fmt"

func main() {
	var n, sum int

	fmt.Scanf("%d", &n)

	if n < 0 {
		n = n * -1
	}

	for n > 0 {
		sum += n % 10
		n = n / 10
	}
	fmt.Println(sum)
}
