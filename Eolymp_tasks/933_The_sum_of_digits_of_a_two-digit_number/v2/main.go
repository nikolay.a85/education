package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%d", &n)

	fmt.Println(sumOfDigits(n))
}

func sumOfDigits(n int) int {
	var sum int
	if n < 0 {
		n = n * -1
	}

	for n > 0 {
		sum += n % 10
		n = n / 10
	}
	return sum
}
