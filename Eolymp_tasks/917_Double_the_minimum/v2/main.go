package main

import "fmt"

func twiceMin(arr []float64) float64 {

	min := arr[0]

	for _, element := range arr {
		if element < min {
			min = element
		}
	}
	return min
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	slice := make([]float64, 0, n)
	for i := 0; i < n; i++ {
		var value float64
		fmt.Scan(&value)
		slice = append(slice, value)
	}
	fmt.Printf("%.2f", twiceMin(slice)*2)
}
