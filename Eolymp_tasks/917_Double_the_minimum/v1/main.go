package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%d", &n)

	slice := make([]float64, 0, n)
	for i := 0; i < n; i++ {
		var value float64
		fmt.Scan(&value)
		slice = append(slice, value)
	}
	min := slice[0]
	for _, element := range slice {
		if element < min {
			min = element
		}
	}
	fmt.Printf("%.2f", min*2)
}
