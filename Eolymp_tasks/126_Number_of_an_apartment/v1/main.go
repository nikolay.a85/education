package main

import "fmt"

func main() {
	var N, P, Q, K int
	fmt.Scanf("%d %d %d %d", &N, &P, &Q, &K)

	nP := 1
	nQ := 1

	for K > N/P {
		nP++
		K -= N / P
	}
	for K > N/P/Q {
		nQ++
		K -= N / P / Q
	}
	fmt.Println(nP, nQ)
}
