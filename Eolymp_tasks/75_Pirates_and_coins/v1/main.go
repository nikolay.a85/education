package main

import "fmt"

func main() {
	var m, a uint64

	fmt.Scanf("%d %d", &a, &m)

	count := 1

	for a*2 != m {
		m -= a
		a++
		count++
	}
	fmt.Println(count)
}
