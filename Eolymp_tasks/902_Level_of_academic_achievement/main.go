package main

import "fmt"

func main() {
	var rating int
	fmt.Scanf("%d", &rating)

	switch {
	case 1 <= rating && rating <= 3:
		fmt.Println("Initial")
	case 4 <= rating && rating <= 6:
		fmt.Println("Average")
	case 7 <= rating && rating <= 9:
		fmt.Println("Sufficient")
	case 10 <= rating && rating <= 12:
		fmt.Println("High")
	default:
		fmt.Println("Unknown rating")
	}
}
