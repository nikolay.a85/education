package main

import "fmt"

func main() {
	var m, n, k int
	fmt.Scanf("%d %d %d", &m, &n, &k)

	fmt.Print(m/n, ".")
	m = (m % n) * 10

	for i := 0; i < k; i++ {
		fmt.Print(m / n)
		m = (m % n) * 10
	}

	fmt.Println()
}
