package main

import (
	"fmt"
	"math"
)

type Point struct {
	x float64
	y float64
}

type Triangle struct {
	a float64
	b float64
	c float64
}

func (t *Triangle) distance(a, b Point) float64 {
	return math.Sqrt((b.x-a.x)*(b.x-a.x) + (b.y-a.y)*(b.y-a.y))
}

func (t *Triangle) perimeter() float64 {
	p := t.a + t.b + t.c

	return p
}

func (t *Triangle) area() float64 {
	p := (t.a + t.b + t.c) / 2
	s := math.Sqrt(p * (p - t.a) * (p - t.b) * (p - t.c))

	return s
}

func main() {
	var a, b, c Point
	fmt.Scanf("%f %f %f %f %f %f", &a.x, &a.y, &b.x, &b.y, &c.x, &c.y)

	var t Triangle
	t.a = t.distance(a, b)
	t.b = t.distance(b, c)
	t.c = t.distance(c, a)

	p := t.perimeter()
	s := t.area()

	fmt.Printf("%.4f %.4f", p, s)
}
