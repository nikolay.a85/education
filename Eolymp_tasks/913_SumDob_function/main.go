package main

import "fmt"

func SumDob(a float64, b float64) (float64, float64) {
	sum := a + b
	dob := a * b
	return sum, dob
}

func main() {
	var n int
	var a, b float64
	fmt.Scanf("%d", &n)

	for i := 0; i < n; i++ {
		fmt.Scan(&a, &b)
		sum, dob := SumDob(a, b)
		fmt.Printf("%.4f %.4f\n", sum, dob)
	}
}
