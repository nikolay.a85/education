package main

import (
	"fmt"
	"math"
)

func height(a, b, c float64) float64 {
	var p, H float64

	p = (a + b + c) / 2
	H = 2 / a * math.Sqrt(p*(p-a)*(p-b)*(p-c))

	return H
}

func main() {
	var a, b, c float64
	fmt.Scanf("%f %f %f", &a, &b, &c)

	fmt.Printf("%.2f %.2f %.2f", height(a, b, c), height(b, a, c), height(c, a, b))
}
