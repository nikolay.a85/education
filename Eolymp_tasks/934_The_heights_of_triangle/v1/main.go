package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c float64
	fmt.Scanf("%f %f %f", &a, &b, &c)

	p := (a + b + c) / 2

	ha := 2 / a * math.Sqrt(p*(p-a)*(p-b)*(p-c))
	hb := 2 / b * math.Sqrt(p*(p-a)*(p-b)*(p-c))
	hc := 2 / c * math.Sqrt(p*(p-a)*(p-b)*(p-c))

	fmt.Printf("%.2f %.2f %.2f", ha, hb, hc)
}
