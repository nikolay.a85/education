package main

import (
	"fmt"
	"math"
)

type triangle struct {
	a float64
	b float64
	c float64
}

func (t *triangle) Height(side float64) float64 {

	p := (t.a + t.b + t.c) / 2
	h := 2 / side * math.Sqrt(p*(p-t.a)*(p-t.b)*(p-t.c))

	return h
}

func main() {
	var t triangle
	fmt.Scanf("%f %f %f", &t.a, &t.b, &t.c)

	fmt.Printf("%.2f %.2f %.2f", t.Height(t.a), t.Height(t.b), t.Height(t.c))
}
