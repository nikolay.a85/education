package main

import (
	"fmt"
	"math"
)

func main() {
	var d, p float64
	fmt.Scanf("%f %f", &d, &p)

	s := d*d + 2*d*math.Sqrt(p*p-d/2*d/2)

	h := math.Sqrt(p*p - (d*d)/2)
	v := d * d / 3 * h

	fmt.Printf("%.3f %.3f", s, v)
}
