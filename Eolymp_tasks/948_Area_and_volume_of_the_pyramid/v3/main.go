package main

import (
	"fmt"
	"math"
)

type pyramid struct {
	a, s float64
}

func (p *pyramid) Area() float64 {
	s := p.a*p.a + 2*p.a*math.Sqrt(p.s*p.s-p.a/2*p.a/2)
	return s
}

func (p *pyramid) Volume() float64 {
	h := math.Sqrt(p.s*p.s - p.a*p.a/2)
	v := p.a * p.a / 3 * h
	return v
}

func main() {
	var d, p float64
	fmt.Scanf("%f %f", &d, &p)
	s := pyramid{d, p}

	fmt.Printf("%.3f %.3f", s.Area(), s.Volume())
}
