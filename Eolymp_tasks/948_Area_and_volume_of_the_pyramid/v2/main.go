package main

import (
	"fmt"
	"math"
)

func main() {
	var d, p float64
	fmt.Scanf("%f %f", &d, &p)

	square := pyramidArea(d, p)

	volume := pyramidVolume(d, p)

	fmt.Printf("%.3f %.3f", square, volume)
}

func pyramidArea(a, b float64) float64 {
	s := a*a + 2*a*math.Sqrt(b*b-a/2*a/2)

	return s
}

func pyramidVolume(a, b float64) float64 {
	h := math.Sqrt(b*b - (a*a)/2)
	v := a * a / 3 * h

	return v
}
