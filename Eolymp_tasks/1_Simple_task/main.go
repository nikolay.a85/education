package main

import "fmt"

func main() {
	var v uint8
	fmt.Scanf("%d", &v)

	fmt.Println(v/10, v%10)
}
