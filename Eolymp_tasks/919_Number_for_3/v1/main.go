package main

import "fmt"

func main() {
	var n, count int
	var totalElements float64
	fmt.Scanf("%d", &n)

	for i := 1; i <= n; i++ {
		var element float64
		fmt.Scan(&element)
		if i%3 == 0 && element > 0 {
			count++
			totalElements = totalElements + element
		}

	}
	fmt.Printf("%d %.2f", count, totalElements)
}
