package main

import "fmt"

func sumAndCountElements(arr []float64) (int, float64) {

	var count int
	var sum float64

	for i := 0; i < len(arr); i++ {
		if (i+1)%3 == 0 && arr[i] > 0 {
			count++
			sum += arr[i]
		}
	}
	return count, sum
}

func main() {

	var n int
	fmt.Scanf("%d", &n)

	slice := make([]float64, 0, n)

	for i := 0; i < n; i++ {
		var value float64
		fmt.Scan(&value)
		slice = append(slice, value)
	}
	a, b := sumAndCountElements(slice)

	fmt.Printf("%d %.2f", a, b)
}
