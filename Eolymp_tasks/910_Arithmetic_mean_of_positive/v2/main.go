package main

import "fmt"

func arithmeticMeanPositive(arr []float64) float64 {
	var total, count, amp float64
	for _, element := range arr {
		if element > 0 {
			total += element
			count++
		}
	}
	amp = total / count
	return amp
}

func main() {
	var n int
	fmt.Scanf("%d", &n)

	slice := make([]float64, 0, n)

	for i := 0; i < n; i++ {
		var x float64
		fmt.Scan(&x)
		slice = append(slice, x)
	}

	res := arithmeticMeanPositive(slice)
	if res > 0 {
		fmt.Printf("%.2f", res)
		return
	}
	fmt.Println("Not found")
}
