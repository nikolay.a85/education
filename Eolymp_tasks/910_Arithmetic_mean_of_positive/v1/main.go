package main

import (
	"fmt"
)

func main() {
	var n, mean, total, count float64
	fmt.Scanf("%f", &n)

	for i := 0; i < int(n); i++ {
		var x float64
		fmt.Scan(&x)
		if x > 0 {
			total += x
			count++
		}
	}
	mean = total / count
	if mean > 0 {
		fmt.Printf("%.2f", mean)
		return
	}
	fmt.Println("Not found")
}
