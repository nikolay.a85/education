package main

import "fmt"

func countRes(iMin, iMax, jMin, jMax uint) int {
	if iMin > iMax {
		iMin, iMax = iMax, iMin
	}
	if jMin > jMax {
		jMin, jMax = jMax, jMin
	}
	countRes := make(map[uint]bool)
	for i := iMin; i <= iMax; i++ {
		for j := jMin; j <= jMax; j++ {
			countRes[i*j] = true
		}
	}
	return len(countRes)
}
func main() {
	var a, b, c, d uint
	fmt.Scanf("%d %d %d %d", &a, &b, &c, &d)

	count := countRes(a, b, c, d)

	fmt.Println(count)
}
