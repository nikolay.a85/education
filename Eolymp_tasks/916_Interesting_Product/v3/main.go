package main

import "fmt"

type value struct {
}

func main() {
	var a, b, c, d uint
	fmt.Scanf("%d %d %d %d", &a, &b, &c, &d)

	if a > b {
		a, b = b, a
	}
	if c > d {
		c, d = d, c
	}
	countRes := make(map[uint]struct{})
	for i := a; i <= b; i++ {
		for j := c; j <= d; j++ {
			countRes[i*j] = struct{}{}
		}
	}
	fmt.Println(len(countRes))
}
