package main

import "fmt"

type rectangle struct {
	a int
	b int
}

func (r *rectangle) perimeter() int {
	return (r.a + r.b) * 2
}

func (r *rectangle) area() int {
	return r.a * r.b
}

func main() {
	var r rectangle
	fmt.Scanf("%d %d", &r.a, &r.b)

	fmt.Println(r.perimeter(), r.area())
}
