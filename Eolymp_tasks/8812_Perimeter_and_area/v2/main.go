package main

import "fmt"

func main() {
	var a, b int

	fmt.Scanf("%d %d", &a, &b)

	fmt.Println(perimeter(a, b), area(a, b))
}
func perimeter(a, b int) int {

	p := 2 * (a + b)
	return p
}

func area(a, b int) int {

	s := a * b
	return s
}
