package main

import "fmt"

func main() {
	var a, b int

	fmt.Scanf("%d %d", &a, &b)

	p := 2 * (a + b)
	s := a * b

	fmt.Println(p, s)
}
