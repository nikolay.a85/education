package main

import (
	"fmt"
	"math"
)

func quadrilateralArea(x1, y1, x2, y2, x3, y3, x4, y4 float64) float64 {
	return math.Round((x1*y2 + x2*y3 + x3*y4 + x4*y1 - x2*y1 - x3*y2 - x4*y3 - x1*y4) / 2)
}

func main() {
	var Ax, Ay, Bx, By, Cx, Cy, Dx, Dy float64
	fmt.Scanf("%f %f %f %f %f %f %f %f", &Ax, &Ay, &Bx, &By, &Cx, &Cy, &Dx, &Dy)

	sABCD := quadrilateralArea(Ax, Ay, Bx, By, Cx, Cy, Dx, Dy)

	fmt.Printf("%.0f", math.Abs(sABCD))
}
