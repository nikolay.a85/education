package main

import (
	"fmt"
	"math"
)

type Point struct {
	x float64
	y float64
}

func newPoint(x, y float64) *Point {
	return &Point{
		x: x,
		y: y,
	}
}

type Quadrilateral struct {
	a, b, c, d Point
}

func newQuadrilateral(a, b, c, d *Point) *Quadrilateral {
	return &Quadrilateral{
		a: *a,
		b: *b,
		c: *c,
		d: *d,
	}
}

func (q *Quadrilateral) area() float64 {
	return math.Round((q.a.x*q.b.y + q.b.x*q.c.y + q.c.x*q.d.y + q.d.x*q.a.y - q.b.x*q.a.y - q.c.x*q.b.y - q.d.x*q.c.y - q.a.x*q.d.y) / 2)
}

func main() {
	var x1, y1, x2, y2, x3, y3, x4, y4 float64
	fmt.Scanf("%f %f %f %f %f %f %f %f", &x1, &y1, &x2, &y2, &x3, &y3, &x4, &y4)

	s := newQuadrilateral(newPoint(x1, y1), newPoint(x2, y2), newPoint(x3, y3), newPoint(x4, y4))

	fmt.Printf("%.0f", math.Abs(s.area()))
}
