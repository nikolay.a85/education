package main

import (
	"fmt"
	"math"
)

func main() {
	var Ax, Ay, Bx, By, Cx, Cy, Dx, Dy float64
	fmt.Scanf("%f %f %f %f %f %f %f %f", &Ax, &Ay, &Bx, &By, &Cx, &Cy, &Dx, &Dy)

	sABCD := math.Round((Ax*By + Bx*Cy + Cx*Dy + Dx*Ay - Bx*Ay - Cx*By - Dx*Cy - Ax*Dy) / 2)

	fmt.Printf("%.0f", math.Abs(sABCD))
}
