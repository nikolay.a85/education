package main

import "fmt"

func main() {
	var lenSlice int
	fmt.Scanf("%d", &lenSlice)

	slice := make([]int, 0, lenSlice)
	for i := 0; i < lenSlice; i++ {
		var v int
		fmt.Scan(&v)
		slice = append(slice, v)
	}
	res := findAndReplaceZeros(slice)
	fmt.Println(res)
}

func findAndReplaceZeros(x []int) []int {
	// свторюємо новий елемент яким будемо заміняти нулі в масиві
	newEl := 1
	// циклом проходимо по масиву для пошуку елементів, які дорівнюють нулю
	for i := 0; i < len(x); i++ {
		if x[i] == 0 {
			for isExist(x, newEl) { // цикл працює поки функція isExist не поверне false
				newEl++
			}
			x[i] = newEl
		}
	}
	return x
}
func isExist(x []int, newEl int) bool {
	for _, el := range x {
		if newEl == el {
			return true
		}
	}
	return false
}
