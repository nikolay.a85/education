package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "HELLO"
	x := []byte(str)
	var s string
	for i := 0; i < len(x); i++ {

		if (i+1)%2 == 0 {
			symbol := string(x[i])
			symbol = strings.ToUpper(symbol)
			s += symbol
		} else {
			s += string(x[i])
		}

	}
	fmt.Println(s)
}
